package com.fueled.restaurantexplorer.network

import com.google.gson.annotations.SerializedName

class ApiResponse(
    @SerializedName("response")
    val venuesResponse: VenuesResponse
)

class VenuesResponse(
    @SerializedName("venues")
    val venues: List<Venue>
)

class Venue(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("location")
    val location: Location
)

class Location(
    @SerializedName("address")
    val address: String?,
    @SerializedName("crossStreet")
    val crossStreet: String?,
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lng")
    val lng: Double,
    @SerializedName("distance")
    val distance: Int,
    @SerializedName("postalCode")
    val postalCode: String?,
    @SerializedName("formattedAddress")
    val formattedAddress: List<String>
)

package com.fueled.restaurantexplorer.network

import io.reactivex.Single
import retrofit2.http.*

private const val CLIENT_ID = "0PZSXA0Y3MEAAV1OF1BSVH2GL1BAWNPIX3KXGOZFSVUTNSAL"
private const val CLIENT_SECRET = "JJALDYJNH3XQWS0Y1OASUFKJPG4A5PW3WLXSHYNZ1YFAXY2V"
private const val CATEGORY_ID_FOOD = "4d4b7105d754a06374d81259"
private const val RESULTS_LIMIT = 50
private const val API_VERSION = "20190803"
private const val QUERY = "restaurant"

interface ApiService {
    @GET(
        "/v2/venues/search?client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET" +
                "&categoryId=$CATEGORY_ID_FOOD" +
                "&limit=$RESULTS_LIMIT" +
                "&v=$API_VERSION" +
                "&query=$QUERY"
    )
    fun getRestaurants(@Query("ll") latLong: String): Single<ApiResponse>
}
package com.fueled.restaurantexplorer.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.fueled.restaurantexplorer.model.RestaurantModel
import io.reactivex.Flowable

@Dao
interface RestaurantDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(models: List<RestaurantModel>)

    @Query("DELETE FROM Restaurant")
    fun deleteAll()

    @Query("SELECT * from Restaurant WHERE id NOT IN (SELECT id FROM BlackListedRestaurant)")
    fun getAllRestaurants(): Flowable<List<RestaurantModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun blackListRestaurant(restaurant: BlackListedRestaurant)
}

package com.fueled.restaurantexplorer.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.model.RestaurantModel

@Database(entities = [RestaurantModel::class, BlackListedRestaurant::class, Office::class], version = 1)
abstract class RestaurantDatabase : RoomDatabase() {

    abstract fun restaurantDao(): RestaurantDao
    abstract fun officeDao(): OfficeDao

    companion object {
        private var INSTANCE: RestaurantDatabase? = null

        fun getDatabase(context: Context): RestaurantDatabase {
            if (INSTANCE == null) {
                synchronized(RestaurantDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            RestaurantDatabase::class.java, "restaurants.db"
                        )
                            .fallbackToDestructiveMigration()
                            .build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}

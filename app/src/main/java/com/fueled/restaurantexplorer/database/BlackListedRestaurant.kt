package com.fueled.restaurantexplorer.database

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Room Entity to store any restaurants that have been thumbs downed
* */
@Entity(tableName = "BlackListedRestaurant")
class BlackListedRestaurant(
        @PrimaryKey
        @NonNull
        @SerializedName("id")
        val id: String,
        @SerializedName("name")
        val name: String)
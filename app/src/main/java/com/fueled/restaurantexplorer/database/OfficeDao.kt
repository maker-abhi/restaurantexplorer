package com.fueled.restaurantexplorer.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.fueled.restaurantexplorer.model.Office
import io.reactivex.Single

@Dao
interface OfficeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(office: Office)

    @Query("DELETE FROM Office")
    fun deleteAll()

    @Query("SELECT * from Office")
    fun getSelectedOffice(): Single<Office>
}

package com.fueled.restaurantexplorer.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fueled.restaurantexplorer.R
import com.fueled.restaurantexplorer.model.RestaurantModel
import com.fueled.restaurantexplorer.databinding.ItemRestaurantBinding

class RestaurantAdapter(private val onBlackListListener: (model: RestaurantModel) -> Unit) :
    RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder>() {

    var restaurants: MutableList<RestaurantModel> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun getItemCount() = restaurants.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        return RestaurantViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_restaurant, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val restaurantModel = restaurants[position]
        holder.binding.restaurant = restaurantModel
        holder.binding.ivThumbsDown.setOnClickListener {
            val adapterPosition = holder.adapterPosition
            if (adapterPosition >= 0) {
                onBlackListListener(holder.binding.restaurant!!)
                restaurants.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
                notifyItemRangeChanged(adapterPosition, restaurants.size)
            }
        }
    }

    class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemRestaurantBinding = DataBindingUtil.bind(itemView)!!
    }
}
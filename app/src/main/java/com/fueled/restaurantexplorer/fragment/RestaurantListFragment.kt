package com.fueled.restaurantexplorer.fragment

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.fueled.restaurantexplorer.adapter.RestaurantAdapter
import com.fueled.restaurantexplorer.model.RestaurantModel
import com.fueled.restaurantexplorer.di.ViewModelFactory
import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.viewmodel.RestaurantsViewModel
import kotlinx.android.synthetic.main.fragment_restaurant_list.*
import javax.inject.Inject
import android.support.v7.widget.DividerItemDecoration
import com.fueled.restaurantexplorer.R

class RestaurantListFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: RestaurantsViewModel
    private lateinit var adapter: RestaurantAdapter

    companion object {
        fun newInstance(): RestaurantListFragment {
            return RestaurantListFragment()
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RestaurantsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_restaurant_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        adapter = RestaurantAdapter(this::onBlackListed)
        rv_restaurants.adapter = adapter
        rv_restaurants.layoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(
            context,
            DividerItemDecoration.VERTICAL
        )
        rv_restaurants.addItemDecoration(dividerItemDecoration)

        office_selector.adapter = ArrayAdapter<Office>(
            office_selector.context,
            android.R.layout.simple_list_item_1,
            viewModel.getOffices()
        )

        swipe_container.setOnRefreshListener {
            viewModel.refreshRestaurants()
        }

        viewModel.selectedOfficeLiveData.observe(this, Observer {
            it?.data?.run {
                office_selector.setSelection(viewModel.getOffices().indexOfFirst { office -> office.name == name })
            }
            office_selector.post {
                initSpinnerListener()
            }
        })

        viewModel.restaurantsLiveData.observe(this, Observer {
            it?.run {
                adapter.restaurants = this.toMutableList()
            }
        })

        viewModel.requestProgressLiveData.observe(this, Observer {
            setSwipeLayoutRefreshState(it ?: false)
        })

        viewModel.errorLiveData.observe(this, Observer {
            if (it.isNullOrEmpty()) return@Observer

            AlertDialog.Builder(context)
                .setMessage("An error occurred.\nDetails:$it")
                .setPositiveButton("OK") { dialog, _ -> dialog.dismiss() }
                .show()

            viewModel.errorLiveData.value = ""
        })
    }

    private fun onBlackListed(restaurantModel: RestaurantModel) {
        viewModel.blackListRestaurant(restaurantModel)
    }

    private fun setSwipeLayoutRefreshState(isRefreshing: Boolean) {
        swipe_container.post {
            swipe_container.isRefreshing = isRefreshing
        }
    }

    private fun initSpinnerListener() {
        office_selector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.setSelectedOffice(viewModel.getOffices()[position])
                office_selector.onItemSelectedListener = null
            }
        }
    }
}
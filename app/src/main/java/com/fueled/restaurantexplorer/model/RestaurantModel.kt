package com.fueled.restaurantexplorer.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import android.support.annotation.NonNull
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "Restaurant")
@Parcelize
class RestaurantModel(
        @PrimaryKey
        @NonNull
        @SerializedName("id")
        val id: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("address")
        val shortAddress: String?,
        @SerializedName("full_address")
        val fullAddress: String?,
        @SerializedName("latitude")
        val latitude: Double,
        @SerializedName("longitude")
        val longitude: Double) : Parcelable
package com.fueled.restaurantexplorer.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "Office")
class Office(
    @PrimaryKey
    val name: String,
    val address: String,
    val latLng: String
    ) {
    override fun toString() = name
}
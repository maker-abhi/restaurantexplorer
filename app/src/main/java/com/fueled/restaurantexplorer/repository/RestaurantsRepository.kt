package com.fueled.restaurantexplorer.repository

import com.fueled.restaurantexplorer.database.BlackListedRestaurant
import com.fueled.restaurantexplorer.database.RestaurantDao
import com.fueled.restaurantexplorer.model.RestaurantModel
import com.fueled.restaurantexplorer.network.ApiService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class RestaurantsRepository(
    private val service: ApiService,
    private val restaurantDao: RestaurantDao
) : IRestaurantsRepository {

    /**
     * Fetch restaurants from the API and update the local database
     * */
    override fun getNearbyRestaurants(latLong: String): Single<List<RestaurantModel>> {
        return service.getRestaurants(latLong)
            .map {
                it.venuesResponse.venues.map { venue ->
                    RestaurantModel(
                        venue.id,
                        venue.name,
                        venue.location.address,
                        venue.location.formattedAddress.joinToString(" "),
                        venue.location.lat, venue.location.lng
                    )
                }
            }
            .doOnSuccess {
                restaurantDao.deleteAll()
                restaurantDao.insertAll(it)
            }
    }

    override fun getRestaurantsFromLocalStorage(): Observable<List<RestaurantModel>> {
        return restaurantDao.getAllRestaurants().toObservable()
    }

    override fun blackListRestaurant(restaurant: RestaurantModel): Completable =
        Completable.fromAction {
            restaurantDao.blackListRestaurant(BlackListedRestaurant(restaurant.id, restaurant.name))
        }
}
package com.fueled.restaurantexplorer.repository

import com.fueled.restaurantexplorer.model.RestaurantModel
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

interface IRestaurantsRepository {
    fun getNearbyRestaurants(latLong: String): Single<List<RestaurantModel>>
    fun getRestaurantsFromLocalStorage(): Observable<List<RestaurantModel>>
    fun blackListRestaurant(restaurant: RestaurantModel): Completable
}
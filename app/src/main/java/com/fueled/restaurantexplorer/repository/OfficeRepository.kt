package com.fueled.restaurantexplorer.repository

import com.fueled.restaurantexplorer.database.OfficeDao
import com.fueled.restaurantexplorer.model.Office
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class OfficeRepository(private val officeDao: OfficeDao) : IOfficeRepository {

    override fun getSelectedOffice(): Single<Office> {
        return officeDao.getSelectedOffice()
    }

    override fun getOffices(): List<Office> {
        // Hardcoded list of Fueled offices. Can be replaced with an API or an asset backed json file.
        return listOf(
            Office("Fueled Noida", "", "28.581612,77.317711"),
            Office("Fueled London", "", "51.520284,-0.070397"),
            Office("Fueled NYC", "", "40.720364,-74.000086"),
            Office("Fueled Austin", "", "30.268728,-97.731585")
        )
    }

    override fun selectOffice(office: Office): Completable {
        return Completable.fromAction {
            officeDao.deleteAll()
            officeDao.insert(office)
        }
    }
}
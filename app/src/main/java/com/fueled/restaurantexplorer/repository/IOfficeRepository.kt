package com.fueled.restaurantexplorer.repository

import com.fueled.restaurantexplorer.model.Office
import io.reactivex.Completable
import io.reactivex.Single

interface IOfficeRepository {
    fun getOffices() : List<Office>
    fun getSelectedOffice() : Single<Office>
    fun selectOffice(office: Office) : Completable
}
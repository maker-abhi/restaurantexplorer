package com.fueled.restaurantexplorer.usecase

import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.repository.IOfficeRepository
import com.fueled.restaurantexplorer.repository.IRestaurantsRepository
import io.reactivex.Completable
import javax.inject.Inject

/**
 * Use case to update the selected office in the database.
 * Also makes the API call again with coordinates of the new office
 *
* */
class SelectOfficeUseCase @Inject constructor(
    private val restaurantsRepository: IRestaurantsRepository,
    private val officeRepository: IOfficeRepository
) {

    fun run(office: Office): Completable {
        return restaurantsRepository.getNearbyRestaurants(office.latLng).ignoreElement()
            .andThen(officeRepository.selectOffice(office))
    }
}
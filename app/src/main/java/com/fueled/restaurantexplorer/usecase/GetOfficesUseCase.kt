package com.fueled.restaurantexplorer.usecase

import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.repository.IOfficeRepository
import io.reactivex.Single
import javax.inject.Inject

class GetOfficesUseCase @Inject constructor(private val officeRepository: IOfficeRepository) {

    fun run(): List<Office> {
        return officeRepository.getOffices()
    }
}
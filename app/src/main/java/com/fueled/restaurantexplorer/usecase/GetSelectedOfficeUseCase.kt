package com.fueled.restaurantexplorer.usecase

import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.repository.IOfficeRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * UseCase to get the selected office for which restaurants are being displayed
 * */
class GetSelectedOfficeUseCase @Inject constructor(private val officeRepository: IOfficeRepository) {

    fun run(): Single<Office> {
        return officeRepository.getSelectedOffice()
    }
}
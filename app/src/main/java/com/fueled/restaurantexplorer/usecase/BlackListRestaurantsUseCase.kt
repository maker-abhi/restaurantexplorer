package com.fueled.restaurantexplorer.usecase

import com.fueled.restaurantexplorer.model.RestaurantModel
import com.fueled.restaurantexplorer.repository.IRestaurantsRepository
import io.reactivex.Completable
import javax.inject.Inject

class BlackListRestaurantsUseCase @Inject constructor(private val restaurantsRepository: IRestaurantsRepository) {

    fun run(restaurantModel: RestaurantModel): Completable {
        return restaurantsRepository.blackListRestaurant(restaurantModel)
    }
}
package com.fueled.restaurantexplorer.usecase

import com.fueled.restaurantexplorer.model.RestaurantModel
import com.fueled.restaurantexplorer.repository.IRestaurantsRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Get restaurants from the database as an Observable stream which would emit again on any changes
 * */
class GetRestaurantsUseCase @Inject constructor(private val restaurantsRepository: IRestaurantsRepository) {

    fun run(): Observable<List<RestaurantModel>> {
        return restaurantsRepository.getRestaurantsFromLocalStorage()
    }
}
package com.fueled.restaurantexplorer.usecase

import com.fueled.restaurantexplorer.repository.IRestaurantsRepository
import io.reactivex.Completable
import javax.inject.Inject

class RefreshRestaurantsUseCase @Inject constructor(private val restaurantsRepository: IRestaurantsRepository) {

    fun run(latLong: String): Completable {
        return restaurantsRepository.getNearbyRestaurants(latLong).ignoreElement()
    }
}
package com.fueled.restaurantexplorer.di

import android.app.Application
import com.fueled.restaurantexplorer.RestaurantExplorerApplication

import com.fueled.restaurantexplorer.di.module.ActivitiesModule
import com.fueled.restaurantexplorer.di.module.ApplicationModule
import com.fueled.restaurantexplorer.di.module.DataModule
import com.fueled.restaurantexplorer.di.module.ViewModelModule

import javax.inject.Singleton

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@Component(
    modules = [
        AndroidInjectionModule::class,
        ApplicationModule::class,
        DataModule::class,
        ViewModelModule::class,
        ActivitiesModule::class]
)
@Singleton
interface AppComponent {

    fun inject(app: RestaurantExplorerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}

package com.fueled.restaurantexplorer.di.module

import com.fueled.restaurantexplorer.di.scope.FragmentScope
import com.fueled.restaurantexplorer.fragment.RestaurantListFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface MainActivityModule {

    @ContributesAndroidInjector
    @FragmentScope
    fun contributeRestaurantListFragment(): RestaurantListFragment

}

package com.fueled.restaurantexplorer.di.module

import com.fueled.restaurantexplorer.MainActivity
import com.fueled.restaurantexplorer.di.scope.ActivityScope

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivitiesModule {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    @ActivityScope
    fun contributeMainActivity(): MainActivity
}

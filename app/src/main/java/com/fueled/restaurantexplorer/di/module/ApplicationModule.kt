package com.fueled.restaurantexplorer.di.module

import com.fueled.restaurantexplorer.database.OfficeDao
import com.fueled.restaurantexplorer.database.RestaurantDao
import com.fueled.restaurantexplorer.network.ApiService
import com.fueled.restaurantexplorer.repository.IOfficeRepository
import com.fueled.restaurantexplorer.repository.IRestaurantsRepository
import com.fueled.restaurantexplorer.repository.OfficeRepository
import com.fueled.restaurantexplorer.repository.RestaurantsRepository

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideRestaurantsRepository(apiService: ApiService, restaurantDao: RestaurantDao): IRestaurantsRepository {
        return RestaurantsRepository(apiService, restaurantDao)
    }

    @Provides
    @Singleton
    fun provideOfficeRepository(officeDao: OfficeDao): IOfficeRepository {
        return OfficeRepository(officeDao)
    }
}

package com.fueled.restaurantexplorer.di.module

import android.app.Application
import com.fueled.restaurantexplorer.database.OfficeDao
import com.fueled.restaurantexplorer.database.RestaurantDao
import com.fueled.restaurantexplorer.database.RestaurantDatabase
import com.fueled.restaurantexplorer.network.ApiService
import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
class DataModule {
    @Provides
    @Singleton
    fun provideApiService(): ApiService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .baseUrl("https://api.foursquare.com/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideRestaurantDao(application: Application): RestaurantDao {
        return RestaurantDatabase.getDatabase(application).restaurantDao()
    }

    @Provides
    @Singleton
    fun provideOfficeDao(application: Application): OfficeDao {
        return RestaurantDatabase.getDatabase(application).officeDao()
    }
}

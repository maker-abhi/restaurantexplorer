package com.fueled.restaurantexplorer.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.model.RestaurantModel
import com.fueled.restaurantexplorer.model.Resource
import com.fueled.restaurantexplorer.usecase.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

const val FUELED_NOIDA_LAT_LONG = "28.581612,77.317711"

class RestaurantsViewModel @Inject constructor(
    private val getRestaurantsUseCase: GetRestaurantsUseCase,
    private val refreshRestaurantsUseCase: RefreshRestaurantsUseCase,
    private val blackListRestaurantsUseCase: BlackListRestaurantsUseCase,
    private val getSelectedOfficeUseCase: GetSelectedOfficeUseCase,
    private val getOfficesUseCase: GetOfficesUseCase,
    private val selectOfficeUseCase: SelectOfficeUseCase
) :
    ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    val restaurantsLiveData = MutableLiveData<List<RestaurantModel>>()

    val requestProgressLiveData = MutableLiveData<Boolean>()

    val selectedOfficeLiveData = MutableLiveData<Resource<Office>>()

    val errorLiveData = MutableLiveData<String>()

//    Load the restaurants from db and the previously selected office(if any)
    init {
        loadRestaurants()
        loadSelectedOffice()
    }

    private fun loadRestaurants() {
        compositeDisposable.add(
            getRestaurantsUseCase.run()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { restaurants -> restaurantsLiveData.value = restaurants }
        )
    }

    private fun loadSelectedOffice() {
        compositeDisposable.add(
            getSelectedOfficeUseCase.run()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { selectedOffice ->
                        selectedOfficeLiveData.value = Resource.success(selectedOffice)
                        // fetch new records from API once office is selected
                        refreshRestaurants()
                    },
                    {
                        selectedOfficeLiveData.value = Resource.error(null)
                        // If no office is selected, fetch records for default office
                        refreshRestaurants()
                    }
                )
        )
    }

    fun refreshRestaurants() {
        requestProgressLiveData.value = true
        // Default office = Noida
        val latLng = selectedOfficeLiveData.value?.data?.latLng ?: FUELED_NOIDA_LAT_LONG
        compositeDisposable.add(
            refreshRestaurantsUseCase.run(latLng)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ requestProgressLiveData.value = false }, {
                    requestProgressLiveData.value = false
                    errorLiveData.value = it.message
                    it.printStackTrace()
                })
        )
    }

    fun blackListRestaurant(restaurantModel: RestaurantModel) {
        compositeDisposable.add(
            blackListRestaurantsUseCase.run(restaurantModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {
                    errorLiveData.value = it.message
                    it.printStackTrace()
                })
        )
    }

    /**
     * This is to be called when changing the selected office.
     * */
    fun setSelectedOffice(office: Office) {
        requestProgressLiveData.value = true
        compositeDisposable.add(
            selectOfficeUseCase.run(office)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        requestProgressLiveData.value = false
                        selectedOfficeLiveData.value = Resource.success(office)
                    },
                    {
                        requestProgressLiveData.value = false
                        it.printStackTrace()
                        errorLiveData.value = it.message
                        selectedOfficeLiveData.value = Resource.error(null, selectedOfficeLiveData.value?.data)
                    }
                )
        )
    }

    fun getOffices(): List<Office> = getOfficesUseCase.run()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}

package com.fueled.restaurantexplorer

import com.fueled.restaurantexplorer.model.Office
import com.fueled.restaurantexplorer.repository.IOfficeRepository
import com.fueled.restaurantexplorer.repository.IRestaurantsRepository
import com.fueled.restaurantexplorer.usecase.SelectOfficeUseCase
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.net.SocketTimeoutException

class SelectOfficeUseCaseTests {

    private lateinit var selectOfficeUseCase: SelectOfficeUseCase

    private val restaurantsRepository = mockk<IRestaurantsRepository>(relaxed = true)
    private val officeRepository = mockk<IOfficeRepository>(relaxed = true)

    @Before
    fun setup() {
        selectOfficeUseCase = SelectOfficeUseCase(restaurantsRepository, officeRepository)
    }

    @Test
    fun run_callsBothRepositoryMethods() {
        val latLng = "28.581612,77.317711"
        every { restaurantsRepository.getNearbyRestaurants(any()) } returns Single.just(emptyList())
        every { officeRepository.selectOffice(any()) } returns Completable.complete()

        val office = Office("", "", latLng)
        val testObserver = selectOfficeUseCase.run(office).test()

        testObserver.assertComplete()
        testObserver.assertNoErrors()

        verify(exactly = 1) { restaurantsRepository.getNearbyRestaurants(latLng) }
        verify(exactly = 1) { officeRepository.selectOffice(office) }
    }

    @Test
    fun run_errorsOnNetworkCallError() {
        val latLng = "28.581612,77.317711"
        val exception = SocketTimeoutException()
        every { restaurantsRepository.getNearbyRestaurants(any()) } returns Single.error(exception)
        every { officeRepository.selectOffice(any()) } returns Completable.complete()

        val office = Office("", "", latLng)
        val testObserver = selectOfficeUseCase.run(office).test()

        testObserver.assertNotComplete()
        testObserver.assertError(exception)
    }
}